package com.zenika.blog.application;

import com.zenika.blog.domain.model.article.Article;
import com.zenika.blog.domain.model.article.BadLengthSummaryException;
import com.zenika.blog.domain.model.user.User;
import com.zenika.blog.domain.model.user.UserNotFoundException;
import com.zenika.blog.domain.repository.ArticleRepository;
import org.checkerframework.checker.fenum.qual.AwtAlphaCompositingRule;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;

import javax.swing.text.html.Option;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;

@SpringBootTest
@ActiveProfiles("test")
public class ArticleServiceTest {

    @MockBean
    UserService userService;

    @MockBean
    ArticleRepository articleRepository;

    @Autowired
    ArticleService articleService;

    @Test
    @WithMockUser(username = "amandine")
    void testCreateArticleOk() throws UserNotFoundException, BadLengthSummaryException {
        //Mock UserService
        Mockito.when(userService.getUserByUsername("amandine")).thenReturn(Optional.of(new User("456", "amandine@mail.com", "amandine", "password")));
        //Mock ArticleRepository
        Article article = new Article("1234", "test", "summary", "content", null);
        Mockito.when(articleRepository.save(any(Article.class))).thenReturn(article);

        //test CreateArticle
        Article createdArticle = articleService.createArticle(article.getTitle(), article.getSummary(), article.getContent());
        assertEquals(createdArticle.getId(), article.getId());
    }

    @Test
    @WithMockUser(username = "amandine")
    void testCreateArticleBadLengthSummary() {
        //Mock UserService
        Mockito.when(userService.getUserByUsername(any(String.class))).thenReturn(Optional.of(new User("456", "amandine@mail.com", "amandine", "password")));
        //Mock ArticleRepository
        Article article = new Article("1234", "test", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque nec placerat neque. " +
                "Nulla iaculis venenatis enim, id congue massa lacinia nec." +
                " Donec vestibulum augue non ligula iaculis, tincidunt pellentesque nibh vehicula. Sed et posuere dui. Fusce id ipsum " +
                "eu arcu consequat semper. Suspendisse ornare consequat ligula, in sodales orci accumsan id.", "content", null);
        Mockito.when(articleRepository.save(any(Article.class))).thenReturn(article);

        //Test CreateArticle with BadLengthSummaryException
        //doc: https://www.codejava.net/testing/junit-test-exception-examples-how-to-assert-an-exception-is-thrown
        //avec une lambda
        assertThrows(BadLengthSummaryException.class, () -> {
            articleService.createArticle(article.getTitle(), article.getSummary(), article.getContent());
        });

        //en surchargeant execute()
        /*
        assertThrows(BadLengthSummaryException.class, new Executable() {
                    @Override
                    public void execute() throws Throwable {
                        articleService.createArticle(article.getTitle(), article.getSummary(), article.getContent());
                    }
                });
         */
    }
}
