-- insérer un user
INSERT INTO users(id, email, username, password)
VALUES('de10ec32-862c-4bb6-8f12-0eaf68ab96e4', 'bob@mail.com', 'bob', '$2a$10$C77saFGdYqbl6iGcOZ9FVuTSTQPE6SThlnsyO8hZd3C.ep.9nS1Fu') on conflict do nothing;

-- insérer un article
INSERT INTO articles(id, title, summary, content, user_id)
VALUES('92528b7c-cd9b-471b-be7a-5dcfa5c231d3', 'Exemple', 'résumé', 'contenu','de10ec32-862c-4bb6-8f12-0eaf68ab96e4') on conflict do nothing;