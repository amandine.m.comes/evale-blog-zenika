CREATE EXTENSION IF NOT EXISTS pg_trgm;

CREATE TABLE IF NOT EXISTS users
(
    id         char(36) primary key,
    email       text not null unique,
    username    text not null unique,
    password    text not null
);

CREATE TABLE IF NOT EXISTS articles
(
   id          char(36) primary key,
   title       text not null,
   summary     text not null,
   content     text not null,
   user_id      char(36) REFERENCES users(id)
);


