package com.zenika.blog.web.users;


import com.zenika.blog.application.UserService;
import com.zenika.blog.domain.model.article.Article;
import com.zenika.blog.domain.model.user.User;
import com.zenika.blog.domain.model.user.UserNotFoundException;
import com.zenika.blog.web.articles.ArticleSummaryDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    //lister les users
    @GetMapping()
    public List<CreatedUserDto> listUsers() {
        List<CreatedUserDto> users = new ArrayList<>();
        for(User user : userService.listUsers()) {
            users.add(new CreatedUserDto(user.getId(), user.getEmail(), user.getUsername()));
        }
        return users;
    }

    //get user par username
    @GetMapping("/{username}")
    ResponseEntity <User> findUserByUsername (@PathVariable String username) throws UserNotFoundException {
       return userService.getUserByUsername(username).map(ResponseEntity::ok).orElseThrow(UserNotFoundException::new);

    }

    @GetMapping("/id/{id}")
    ResponseEntity <User> findUserById (@PathVariable String id) throws UserNotFoundException {
        return userService.getUserBId(id).map(ResponseEntity::ok).orElseThrow(UserNotFoundException::new);
    }

    //pour vérifier si le user existe en base
    @PostMapping("/me")
    @ResponseStatus(value = HttpStatus.OK)
    public String me() {
       return SecurityContextHolder.getContext().getAuthentication().getName();
    }

    //créer un user
    @PostMapping()
    @ResponseStatus(value = HttpStatus.CREATED)
    public CreatedUserDto createUser(@RequestBody CreateUserDto createUserDto) {
       User user = this.userService.createUser(createUserDto.getEmail(), createUserDto.getUsername(), createUserDto.getPassword());
        return new CreatedUserDto(user.getId(), user.getEmail(), user.getUsername());
    }




}
