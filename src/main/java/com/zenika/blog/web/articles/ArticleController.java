package com.zenika.blog.web.articles;


import com.zenika.blog.application.ArticleService;
import com.zenika.blog.domain.model.article.Article;
import com.zenika.blog.domain.model.article.BadLengthSummaryException;
import com.zenika.blog.domain.model.user.UserNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

//@CrossOrigin (origins = "http://localhost:4200/")
@RestController
@RequestMapping("/articles")
public class ArticleController {

    private final ArticleService articleService;

    @Autowired
    public ArticleController(ArticleService articleService) {
        this.articleService = articleService;
    }

    @PostMapping
    @ResponseStatus(value = HttpStatus.CREATED)
    public Article createArticle(@RequestBody ArticleDetailDto articleDto) throws BadLengthSummaryException, UserNotFoundException {
        //SecurityContextHolder.getContext().getAuthentication().getName();
      return this.articleService.createArticle(articleDto.getTitle(), articleDto.getSummary(), articleDto.getContent());
    }

    @GetMapping()
    @ResponseStatus(value = HttpStatus.OK)
    public Iterable<ArticleSummaryDto> fetchAllArticles(@RequestParam(name="title", required = false) String title){
        Iterable <Article> allArticles = Collections.emptyList();
        if(title != null){
            allArticles = this.articleService.getAllArticleByTitle(title);
        }else{
            allArticles = this.articleService.getAllArticles();
        }
/*
        return StreamSupport.stream(
                        Spliterators.spliteratorUnknownSize(allArticles.iterator(), Spliterator.ORDERED), false)
                .map(a -> new ArticleSummaryDto(a.getId(), a.getTitle(), a.getSummary()))
                .collect(Collectors.toList());

 */
        List<ArticleSummaryDto> articlesDtoFor = new ArrayList();
        for(Article article : allArticles) {
            articlesDtoFor.add(new ArticleSummaryDto(article.getId(), article.getTitle(), article.getSummary()));
        }
        return articlesDtoFor;

    }

    @GetMapping("/{articleId}")
    public ResponseEntity <Article> getOneArticleById(@PathVariable("articleId") String articleId) {
        Optional <Article> foundArticle = this.articleService.getArticle(articleId);
        return foundArticle.map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }


    @DeleteMapping("/{articleId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteArticle(@PathVariable("articleId") String articleId) throws UserNotFoundException {
        articleService.deleteArticle(articleId);
    }


    @PutMapping("/{articleId}")
    @ResponseStatus(HttpStatus.OK)
    public void changeArticleContent(@PathVariable("articleId") String articleId, @RequestBody ArticleDetailDto articleDetailDto) throws UserNotFoundException {
        articleService.changeArticleContent(articleId, articleDetailDto.getContent());
    }

    //afficher les articles d'un user
    @GetMapping("/user/{userid}")
    List <Article> findUserArticles(@PathVariable String userid) {
        return articleService.findArticles(userid);
    }



}
