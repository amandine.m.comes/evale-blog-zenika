package com.zenika.blog.configuration;

import com.zenika.blog.domain.repository.UserRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Configuration
    public class SecurityConfiguration {

        @Bean
        SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
            return http
                    .antMatcher("/**")
                    .csrf().disable() // CSRF protection is enabled by default in the Java configuration.
                    .authorizeRequests(authorize -> authorize
                            .antMatchers(HttpMethod.GET, "/articles").permitAll()
                            .antMatchers(HttpMethod.GET, "/articles/*").permitAll()
                            .antMatchers(HttpMethod.GET, "/users/*").permitAll()
                            .antMatchers(HttpMethod.GET, "/**").permitAll()
                            .antMatchers(HttpMethod.POST, "/users").permitAll()
                            .anyRequest().authenticated()
                    )
                    // Activate httpBasic Authentication https://en.wikipedia.org/wiki/Basic_access_authentication
                    .httpBasic()
                    .and()
                    .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                    .and()
                    .cors()
                    .and()
                    .build();
        }
    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**").allowedOrigins("http://localhost:4200", "http://localhost").allowedMethods("HEAD",
                        "GET", "POST", "PUT", "DELETE", "PATCH");
            }
        };
    }

//    @Bean
//    public UserDetailsService users() {
//        UserDetails amandine = User.builder()
//                .username("amandine")
//                .password("{noop}password")
//                .roles("USER")
//                .build();
//        UserDetails maxime = User.builder()
//                .username("maxime")
//                .password("{bcrypt}$2a$10$cq3mD7wNj7zviWJTZA4ZauuAaqhBjSvdpOh9AsrSMuoXD88.Sifsa")
//                .roles("USER")
//                .build();
//        UserDetails baptiste = User.builder()
//                .username("baptiste")
//                .password("{noop}baptiste")
//                .roles("USER")
//                .build();
//        return new InMemoryUserDetailsManager(amandine, maxime, baptiste);
//    }

    @Bean
    UserDetailsService userDetailsService(UserRepository userRepository) {
            return new UserDetailsService() {
                @Override
                public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
                    return userRepository.findByUsername(username).orElse(null);
                }
            };
    }

    //encoder le password à l'inscription et à l'authentification
    @Bean
    public PasswordEncoder encoder() {
            return new BCryptPasswordEncoder();
    }

    }

