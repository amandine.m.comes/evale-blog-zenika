package com.zenika.blog.application;

import com.zenika.blog.domain.model.article.Article;
import com.zenika.blog.domain.model.user.User;
import com.zenika.blog.domain.repository.ArticleRepository;
import com.zenika.blog.domain.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class UserService {

    private final UserRepository userRepository;
    private PasswordEncoder passwordEncoder;
    private final ArticleRepository articleRepository;

    @Autowired
    public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder, ArticleRepository articleRepository) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.articleRepository = articleRepository;
    }

    public User createUser(String username, String email, String password) {
        User newUser = new User(UUID.randomUUID().toString(), username, email, password);
        newUser.setPassword(passwordEncoder.encode(password));
        userRepository.save(newUser);
        return newUser;
    }

    public Optional<User> getUserByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    public Optional<User> getUserBId(String id) {
        return userRepository.findById(id);
    }

    public Iterable<User> listUsers() {
        return userRepository.findAll();
    }





}

