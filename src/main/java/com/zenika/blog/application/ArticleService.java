package com.zenika.blog.application;

import com.zenika.blog.domain.model.article.Article;
import com.zenika.blog.domain.model.article.BadLengthSummaryException;
import com.zenika.blog.domain.model.user.User;
import com.zenika.blog.domain.model.user.UserNotFoundException;
import com.zenika.blog.domain.repository.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

//annotation service plutôt que component : un service est un component
@Service
public class ArticleService {

    private final ArticleRepository articleRepository;
    private final UserService userService;


   @Autowired
    public ArticleService(ArticleRepository articleRepository, UserService userService) {
        this.articleRepository = articleRepository;
        this.userService = userService;
    }

    public Article createArticle(String title, String summary, String content) throws BadLengthSummaryException, UserNotFoundException {
        String[] words = summary.split(" ");
        int numberOfWords = words.length;
        if (numberOfWords <= 50) {
            Optional<User> userByUsername = userService.getUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
            User user = userByUsername.orElseThrow(UserNotFoundException::new);
            //créer exception usernotfound à la place de la runtime
            Article article = new Article(UUID.randomUUID().toString(), title, summary, content, user);
            article = articleRepository.save(article);
            return article;
        } else {
            throw  new BadLengthSummaryException();
        }
    }

    public Iterable<Article> getAllArticles(){
        return articleRepository.findAll();
    }



    public Optional<Article> getArticle(String articleId){
        return articleRepository.findById(articleId);
    }

    public void deleteArticle(String articleId) throws UserNotFoundException {
        //checker si article existe
        Article article = articleRepository.findById(articleId).orElseThrow();
        //ajout exception not found article
        User user = userService.getUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).orElseThrow(UserNotFoundException::new);
        if (user.getUsername().equals(article.getUser().getUsername())) {
            articleRepository.deleteById(articleId);
        } else {
            throw new RuntimeException("le user n'est pas bon");
        }
    }
//checker nom auteur
    public void changeArticleContent(String articleId, String newContent ) throws UserNotFoundException {
        Article article = articleRepository.findById(articleId).orElseThrow();
        //ajout exception not found article
        User user = userService.getUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).orElseThrow(UserNotFoundException::new);
        // equals : entre des strings
        if(user.getUsername().equals(article.getUser().getUsername())) {
            article.setContent(newContent);
            articleRepository.save(article);
        } else {
            throw new RuntimeException("le user n'est pas bon");
        }
    }

    public Iterable<Article> getAllArticleByTitle(String title){return articleRepository.findByTitle(title);}

    public List<Article> findArticles(String userid) {
        return articleRepository.findByUserId(userid);
    }
}
