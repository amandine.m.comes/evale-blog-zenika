
package com.zenika.blog.domain.model.article;


import com.zenika.blog.domain.model.user.User;

import javax.persistence.*;

@Entity
@Table(name="articles")
@Access(AccessType.FIELD)
public class Article {

    @Id
    private String id;
    private String title;
    private String summary;
    private String content;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User user;

    protected Article(){
        //FOR JPA
    }

    public Article(String id, String title, String summary, String content, User user) {
        this.id = id;
        this.title = title;
        this.summary = summary;
        this.content = content;
        this.user = user;
    }

    public String getId() {
        return this.id;
    }

    public String getTitle() {
        return this.title;
    }

    public String getSummary() {
        return this.summary;
    }

    public String getContent() {
        return this.content;
    }

    public User getUser() {
        return user;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

}



