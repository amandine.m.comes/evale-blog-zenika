package com.zenika.blog.domain.repository;

import com.zenika.blog.domain.model.article.Article;
import com.zenika.blog.domain.model.user.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User, String> {

    public Optional<User> findByUsername(String username);
}
