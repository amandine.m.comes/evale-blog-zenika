package com.zenika.blog.domain.repository;

import com.zenika.blog.domain.model.article.Article;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
@Profile("!test")
public interface ArticleRepository extends CrudRepository<Article, String> {

    @Query(value = "SELECT *\n" +
            "FROM articles\n" +
            "WHERE word_similarity(?1, articles.title) > 0.2", nativeQuery = true)
    public Iterable <Article> findByTitle(String title);

    public List <Article> findByUserId(String userid);
}
